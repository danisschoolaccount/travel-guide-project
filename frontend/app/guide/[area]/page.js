import Image from 'next/image';
import dynamic from "next/dynamic";
import { FaQuestion } from 'react-icons/fa6';
import { Allow } from './meta';

export const metadata = {
    title: 'Guide - DGEA',
};

export default async function GuidePage({ params }) {
    const { allow, general, about: About, majorCities: MajorCities, majorFigures: MajorFigures } = await getData(params);
    const Icon = general?.icon || FaQuestion;

    if (!allow) {
        return (
            <>
                <div className="flex flex-col items-center py-24">
                    <h1 className="text-6xl font-bold">404</h1>
                    <h2 className="text-2xl font-semibold">This page does not exist.</h2>
                </div>
            </>
        )
    }

    return (
        <>
            <div className="relative min-h-36 max-h-48 h-48 w-full border-b-2 border-gray-300">
                <div className="absolute top-0 left-8 flex flex-row gap-x-4 items-center z-20 h-full">
                    <Icon className="text-4xl text-gray-200" />
                    <div className="flex flex-col gap-y">
                        <h1 className="text-2xl font-bold">{general?.title || "Unknown"}</h1>
                        <h2 className='text-xl font-semibold'><b>Region</b> {general?.region || "Unknown"} &middot; <b>Tender</b> {general?.tender || "Unknown"}</h2>
                    </div>
                </div>
                {
                    general?.imgSrc && (
                        <Image src={general?.imgSrc} width={2009} height={2009} className="w-full h-full object-cover bg-black brightness-[0.25]" />
                    )
                }
            </div>
            <div className="flex flex-col gap-y-8 px-16 md:px-32 py-8 md:items-center">
                <div className='flex flex-col gap-y-4 md:max-w-[75vw]'>
                    <div className="flex flex-col gap-y-2">
                        <span className="text-3xl font-semibold">About</span>
                        <About />
                    </div>
                    <div className="flex flex-col gap-y-2">
                        <span className="text-3xl font-semibold">Major cities</span>
                        <MajorCities />
                    </div>
                    <div className="flex flex-col gap-y-2">
                        <span className="text-3xl font-semibold">Major figures</span>
                        <MajorFigures />
                    </div>
                </div>
            </div>
        </>
    )
};

export const getData = async ({ area }) => {
    // check if area is allowed
    if (!Allow.includes(area)) return { allow: false, general: null, about: null, majorCities: null, majorFigures: null };
    
    const general = await import(`./meta/${area}/general.js`).then((mod) => mod.default).catch(() => {});
    const about = dynamic(() => import(`./meta/${area}/about.mdx`));
    const majorCities = dynamic(() => import(`./meta/${area}/majorCities.mdx`));
    const majorFigures = dynamic(() => import(`./meta/${area}/majorFigures.mdx`));

    return {
        allow: true,
        general,
        about,
        majorCities,
        majorFigures
    };
};