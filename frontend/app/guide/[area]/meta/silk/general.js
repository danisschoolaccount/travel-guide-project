import { GiCamel } from "react-icons/gi";

export const Metadata = {
    title: "The Silk Road (the Silk Routes)",
    imgSrc: "/img/silk/cover.jpg",
    region: "Asia",
    tender: "Paper money",
    colour: "#532d41",
    icon: GiCamel
};

export default Metadata;