import { GiAfrica, GiCamel } from "react-icons/gi";

export const Metadata = {
    title: "Trans-Saharan Trade Route",
    imgSrc: "/img/tstr/cover.jpg",
    region: "Africa",
    tender: "Gold, salt, cowrie shells",
    colour: "#967f27",
    icon: GiAfrica
};

export default Metadata;