import { GiAfrica, GiCamel, GiIndiaGate } from "react-icons/gi";

export const Metadata = {
    title: "Indian Ocean Trade Network",
    imgSrc: "/img/indian/cover.jpg",
    region: "The Indies",
    tender: "Larin?",
    colour: "#2a483e",
    icon: GiIndiaGate
};

export default Metadata;