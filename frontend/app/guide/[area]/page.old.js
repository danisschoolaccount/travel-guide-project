import Image from 'next/image';
import { GiCamel } from 'react-icons/gi';

export default function SilkRoad() {
    return (
        <>
            <div className="relative h-36 w-full border-b-2 border-gray-300">
                <Image src="/img/silk.jpg" width={2009} height={2009} className="w-full h-full object-cover blur-sm" />
            </div>
            <div className="flex flex-col gap-y-8 px-16 py-8">
                <div className="flex flex-row gap-x-4 items-center">
                    <GiCamel className="text-4xl text-gray-200" />
                    <div className="flex flex-col gap-y">
                        <h1 className="text-2xl font-bold">Silk Road</h1>
                        <h2 className='text-xl font-semibold'>Asia &middot; Paper money</h2>
                    </div>
                </div>
                <div className='flex flex-col gap-y-4'>
                    <div className="flex flex-col gap-y-2">
                        <span className="text-2xl font-semibold">About</span>
                        <p>to do</p>
                    </div>
                    <div className="flex flex-col gap-y-2">
                        <span className="text-2xl font-semibold">Major cities</span>
                        <p>to do</p>
                    </div>
                </div>
            </div>
        </>
    )
}
