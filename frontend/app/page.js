import Image from 'next/image'
import Link from 'next/link'
import { Allow } from './guide/[area]/meta';

export default async function Home() {
    const data = await getData();
    return (
        <>
            <div className="relative min-h-36 max-h-72 h-72 w-full border-b-2 border-gray-300">
                <div className="absolute top-0 md:left-8 flex flex-row gap-x-4 items-center z-20 h-full">
                    <div className="flex flex-col gap-y-2 md:gap-y-0 text-center md:text-left">
                        <h1 className="text-4xl font-bold">Choose a pathway</h1>
                        <h2 className="text-xl md:text-2xl font-medium">To get started, click one of the buttons below to access a section of the guide.</h2>
                    </div>
                </div>
                <Image src={"/img/boat.jpg"} width={2009} height={2009} className="w-full h-full object-cover bg-black brightness-[0.25]" />
            </div>
            <div className="flex flex-col gap-y-8 p-16 items-center text-center md:text-left">
                <div className="flex flex-col gap-y-8 md:flex-row md:gap-x-16">
                    {
                        data.map((area) => {
                            return (
                                <Link href={`/guide/${area.area}`} key={area.area} style={{
                                    backgroundColor: area.colour || "#000000"
                                }} className="flex flex-col items-center justify-center gap-y-2 hover:brightness-90 text-white font-bold py-4 px-8 rounded-lg text-center">
                                    <area.icon className="text-4xl" /> 
                                    <span className="text-2xl">{area.title}</span>
                                </Link>
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
};

export const getData = async () => {
    // check if area is allowed
    let data = [];
    for (let area of Allow) {
        const general = await import(`./guide/[area]/meta/${area}/general.js`).then((mod) => mod.default).catch(() => {});
        data.push({
            area,
            ...general
        });
    };

    return data;
};