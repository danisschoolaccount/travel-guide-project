import './globals.css';
import { Inter } from 'next/font/google';
import Link from 'next/link';
import { FaFaceSmileWink, FaSailboat } from 'react-icons/fa6';

const inter = Inter({ subsets: ['latin'], variable: "--font-inter" })

export const metadata = {
  title: 'Dani\'s Guide to Eurasia',
  description: 'Generated by create next app',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={`${inter.variable} font-sans text-white`}>
        <nav className="flex flex-row gap-x-4 items-center justify-between px-4 py-2 w-full bg-blue-950">
            <Link href="/" className="flex flex-row items-center gap-x-2 text-white hover:text-gray-200 font-bold">
                {metadata.title}
                <FaSailboat className="text-xl" />
            </Link>
            <div className="flex flex-row gap-x-4 items-center">
                <Link href="/" className='text-white hover:text-gray-200 font-bold'>Home</Link>
                <Link href="/guide/silk" className='hidden md:block text-white hover:text-gray-200 font-bold'>Silk Road</Link>
                <Link href="/guide/tstr" className='hidden md:block text-white hover:text-gray-200 font-bold'>Trans-Saharan</Link>
                <Link href="/guide/indian" className='hidden md:block text-white hover:text-gray-200 font-bold'>Indian Ocean</Link>
            </div>
        </nav>
        <main className="min-h-screen">{children}</main>
        <div className="flex flex-col w-full bg-black p-8">
            <span className="text-white font-bold">created by dani</span>
            <span className="text-white font-bold">check out some of my other work <Link href="https://danny.works" target="_blank" rel="noreferrer noopener" className="text-blue-300 hover:text-blue-400">here</Link></span>
            <span className="text-white font-bold text-sm">this isn&apos;t really that much work frontend-wise, but i spent lots of time developing a system that dynamically compiles all this data from markdown files LOL</span>
        </div>
      </body>
    </html>
  )
}
